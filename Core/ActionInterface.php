<?php

namespace Core;

interface ActionInterface {

    /**
     * Dispatch request
     */
    public function execute();
}