<?php
namespace Model;
/**
 * Class AbstractModel
 *
 * This class it's basic realisation of common opportunities for model
 * The model must implements access to data as array.
 * There is no implementation, but it is supposed
 */
abstract class AbstractModel
{

    /**
     * Resource model instance
     * Provide access to DB, load, save
     * Save means insert for new rows and update for exits
     */
    protected $_resource;

    /**
     * Event Dispatcher
     * Necessary for event initialization in Observer implementation
     */
    protected $_eventManager;


    /**
     * Save object data
     *
     */
    public function save()
    {
        $this->_validateBeforeSave();
        $this->_beforeSave();
        $this->_getResource()->save($this);
        $this->_afterSave();
        return $this;
    }


    /**
     * @param $modelId
     * @param null $field
     * @return $this
     */
    public function load($modelId, $field = null)
    {

        $this->_getResource()->load($this, $modelId, $field);
        return $this;
    }


    /**
     * @return ResourceModel
     */
    protected function _getResource()
    {
        if (empty($this->_resource)) {

            /*
             *  create instance
             *  throw Exception if error
             */
        }

        return $this->_resource;
    }

    /**
     * Processing object before save data
     * @return $this
     */
    protected function _beforeSave()
    {
        return $this;
    }
    /**
     * Processing object after save data
     * @return $this
     */
    protected function _afterSave()
    {
        return $this;
    }

    /**
     * Validate data before save
     * @return $this
     */
    abstract protected function _validateBeforeSave();


}