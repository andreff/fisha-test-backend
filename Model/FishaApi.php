<?php

namespace Model;


class FishaApi {

    /**
     *
     * @var Specific ApiClient for transport (Curl, SOAP etc)
     */
    protected $apiClient;

    public function __construct()
    {
        $this->apiClient = new SpecificApiClient();

        /*
         * do somesing with apiClient if it needed
         *
         * authenticate, set params etc
         *
         */

    }


    /**
     * @param $shopperEmail
     * @return mixed
     */
    public function getShopperByEmail($shopperEmail)
    {
        return $this->sendRequest('getShopperById',['email'=>$shopperEmail]);
    }


    /**
     * @param Shopper $shopper
     * @return mixed
     */
    public function createNewShopper(\Model\Shopper $shopper)
    {

        $data = $this->prepareShopperData($shopper);

        return $this->sendRequest('CreateNewShopper', $data);

    }

    /**
     * @param Shopper $shopper
     * @return array
     */
    public function updateShopper(\Model\Shopper $shopper)
    {

        $data = $this->prepareShopperData($shopper);

        return $this->sendRequest('UpdateShopper', $data);

    }


    /**
     * @param Shopper $shopper
     * @return array|bool
     */
    public function sendShopperToErp(\Model\Shopper $shopper)
    {

        $fishaShopperId = false;

        $shopperExists = $this->getShopperByEmail($shopper->getEmail());

        if(is_array($shopperExists) && isset($shopperExists['email'])) {

            $fishaShopperId = $this->updateShopper($shopper);

        } else {

            $fishaShopperId = $this->createNewShopper($shopper);
        }

        return $fishaShopperId;
    }

    /**
     * @param Order $order
     * @return mixed
     * @throws \Exception
     */
    public function sendOrderToErp(\Model\Order $order)
    {
        /*
         * retrive \Model\Shopper $shopper  from $order
         */
        $shopper = $order->getShopper();

        $fishaShoperId = $this->getShopperByEmail($shopper->getEmail());

        if(!$fishaShoperId) {
            $fishaShopperId = $this->createNewShopper($shopper);
        }

        if(!$fishaShoperId) {
            throw new \Exception(sprintf('Can\'t get fishaShopperId for Shopper ID %s in Order ID %s',$shopper->getId(), $order->getId() ));
        }

        $data = $this->prepareOrderData($order);
        $data['fishaShoperId'] = $fishaShoperId;

        return $this->sendRequest('createNewOrder', $data);

    }

    /**
     * @param $action
     * @param array $data
     * @return mixed
     */
    protected function sendRequest($action, array $data)
    {
        $response = [];

        $response = $this->apiClient->sendRequest($action, $data);

        return $this->prepareResponse($response);
    }

    /**
     * @param $response
     * @return mixed
     */
    protected function prepareResponse($response)
    {
        /*
         * analyse response
         *
         * if it contains errors - log it, throw Exception etc
         *
         * Returns the required part of the query
         */

        return $response;

    }

    /**
     * Create array for API Request
     * @param Shopper $shopper
     * @return array
     */
    protected function prepareShopperData(\Model\Shopper $shopper)
    {
        return  [
            'email'=>$shopper->getEmail(),
            'name'=>$shopper->getName(),
            'lastName'=>$shopper->getLastName(),
            'phone'=>$shopper->getPhone(),
            'city'=>$shopper->getCity(),
            'street'=>$shopper->getStreet(),
            'houseNumber'=>$shopper->getHouseNumber(),
        ];
    }

    /**
     * Create array for API Request
     * @param Order $order
     * @return array
     */
    protected function prepareOrderData(\Model\Order $order)
    {
        return  [
            'orderId'=>$order->getId(),
            'orderTotal'=>$order->getOrderTotal(),
            'token'=>$order->getPaymentToken(),
        ];
    }


}