<?php

namespace Model;

class Order extends AbstractModel
{


    public function getShopperOrders(Shopper $shopper)
    {

        $shopperId = $shopper->getId();


        if((int)$shopperId < 1) {
            throw new \Exception('Only authorized users can access to orders');
        }

        $collection = $this->load($shopperId, 'shopper_id'); //

        return $collection;
    }

    /**
     * @return $this
     */
    protected function _afterSave()
    {
        $this->_eventManager->dispatch('order_save_after', ['order'=>$this]);
        return $this;
    }

    
    /**
     *  @return $this
     */
    protected function _validateBeforeSave()
    {
        /*
         *
         * Validate data before save
         *
         * throw Exception if error
         *
         */

        return $this;
    }

}