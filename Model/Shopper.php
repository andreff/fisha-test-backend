<?php
namespace Model;

class Shopper extends AbstractModel
{

    /**
     * @param $id
     * @return $this
     */
    public function getShopperById($id)
    {
        return $this->load($id);
    }

    protected function _afterSave()
    {
        $this->_eventManager->dispatch('shopper_save_after', ['shopper'=>$this]);
        return $this;
    }


    /**
     *  @return $this
     */
    protected function _validateBeforeSave()
    {
        /*
         *
         * Validate data before save
         *
         * throw Exception if error
         *
         */

        return $this;
    }


}