<?php

namespace Observer;


class OrderAfterSave {

    /**
     * Some logger which can write to logfile
     * @var Logger
     */
    protected $logger;

    /**
     * @param $object
     */
    public function processShopper($object)
    {

        /**
         * Here it is better to add order to the queue for batch processing
         * This will speed up the processing of order data
         * But call fisha API here
         */

        $order = $object->getOrder();

        $this->sendShopperToErp($order);

    }


    protected function sendOrderToErp(\Model\shopper $order)
    {
        $this->logger->log(sprintf('Start ERP processing Order ID %s',$order->getId()));

        try {

            if((int)$order->getId() < 1) {

                throw new \Exception('Order ID does not exists' );
            }

            /*
             * call api
             */


            $api = new \Model\FishaApi();

            $fishaOrderId = $api->sendShopperToErp($order);

            if(!$fishaOrderId) {
                throw new \Exception('fiShaOrderID does not exists' );
            }

        } catch (\Exception $e) {

            $this->logger->log(sprintf('ERROR processing Order ID %s',$order->getId()));
            $this->logger->log($e);

            /*
             * send message to admin etc
             */
        }

        $this->logger->log(sprintf('%s End ERP processing Shopper ID %s',$shopper->getId()));

    }


}