<?php
namespace Observer;

/**
 * Class shopperAfterSave
 * @package Observer
 */
class ShopperAfterSave {

    /**
     * Some logger which can write to logfile
     * @var Logger
     */
    protected $logger;

    /**
     * @param $object
     */
    public function processShopper($object)
    {

        /**
         * Here it is better to add shopper to the queue for batch processing
         * This will speed up the processing of shopper data
         * But call fisha API here
         */

        $shopper = $object->getShopper();

        $this->sendShopperToErp($shopper);

    }

    /**
     * @param \Model\Order $shopper
     */
    protected function sendShopperToErp(\Model\Order $shopper)
    {
        $this->logger->log(sprintf('Start ERP processing Shopper ID %s',$shopper->getId()));

        try {

            if((int)$shopper->getId() < 1) {

                throw new \Exception('Shopper ID does not exists' );
            }

            /*
             * call api
             */
            $api = new \Model\FishaApi();

            $fishaShopperId = $api->sendShopperToErp($shopper);

            if(!$fishaShopperId){
                throw new \Exception('Empty fishaShopperId see log file for details' );
            }

        } catch (\Exception $e) {

            /*
             * send message to admin etc
             */

            $this->logger->log(sprintf('ERROR processing Shopper ID %s',$shopper->getId()));
            $this->logger->log($e);


        }

        $this->logger->log(sprintf('%s End ERP processing Shopper ID %s',$shopper->getId()));

    }


}